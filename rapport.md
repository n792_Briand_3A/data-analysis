# Data Analysis & classification

##### Objectif du projet : Détecter les types de battements de coeur

##### Objectif du rapport : Expliquer les résultats, expliquer les méthodes

Données disponibles :

1 ligne correspond à un segment = un enregistrement d'ECG

on prend 20 segments pour l'apprentissage et 10 segments pour la base de test et 3 classes

#### Première partie : implémenter la DTW pour être capable d'identifier les classes des enregistrements de tests

Pour commencer, on a implémenter l'algorithme DTW de manière assez classique, sans fenêtre de distance, avec des poids de 1. 

Sur un jeu de donnée de 10 segments de test, 20 segments d'apprentissage, et 3 classes.

![weightedOne](./img/weightedOne.png)

Au final, on a 0.63 d'accuracy score, il faut se rendre compte qu'on a un tel résultat car le profil médian de chaque classe est assez proche les uns des autres. Cependant, on l'air d'avoir un bon résultat pour reconnaitre juste les battements normaux (0.75), donc il faudrait voir avec différents nombres de classes. (On peut appliquer ce raisonnement à toutes les autres méthodes ci-après)

#### Deuxième partie : classification par Analyse en composante principale et kppv

##### ACP :

On veut réduire la dimension des valeurs en gardant un maximum de représentabilité des classes via la méthode ACP, ici avec 5 classes représentées en 3 dimensions

Ratio de variance conservé par axe : 0.48 ; 0.11 ; 0.08

Ce qui donne au total, 0.67, c'est acceptable, mais on va augmenter les dimensions jusqu'à atteindre plus de 90% de qualité, et constater l'évolution sur le reste. voir 

![output_acp_3D](img/output_acp_3D.png)

##### K Plus Proche Voisins :

On prend la nouvelle représentation des données et on utilise l'algorithme des kkpv pour définir de nouvelles classes en fonction de la distance

Accuracy : 0.5

![output_knn_3d_1n](img/output_knn_3d_1n.png)

Globalement réduire à 3 dimensions permet de réduire les temps de traitement et une bonne représentation graphique, mais c'est moins précis. Avec 42 dimensions on est forcément beaucoup plus long mais beaucoup plus précis (voir tableau dans quatrième partie)

#### Troisième partie : forêts aléatoire + réseaux de neurones

##### Forêts aléatoires

La fonction procurée a quelques paramètres sur lesquels on peut jouer pour maximiser notre score de prédiction, `max_depth`, `n_estimators` et `max_features` voici le résultat par défaut, puis nous allons réaliser une analyse plus poussée dans la partie 4, sachant que pour paramètres fixés, on peut avoir des résultats différents. La complexité annoncée est un `O(M*N*log(N))` avec M le nombre d'arbres et N le nombre de samples effectués

`max_depth=None`, `n_estimators=100` et `max_features='auto'`  => score = 0.62

![output_falea_None_100_auto](/home/n7student/3A/DataAnalysis/data-analysis/img/output_falea_None_100_auto.png)

##### Réseaux de neurones

En utilisant ces paramètres par défaut, à part le solveur car lbfgs est plus optimisé sur des petites bases qu'adam `random_state=1, max_iter=300E0, tol=1E-4, solver='lbfgs'`

accuracy = 0.68

![output_neurone_300_1e-4_lbfgs](/home/n7student/3A/DataAnalysis/data-analysis/img/output_neurone_300_1e-4_lbfgs.png)

Ce qu'on va vouloir faire ensuite c'est jouer sur les paramètres pour tenter d'améliorer notre précision.

Par exemple, on augmente le nombre d'iterations et réduit la tolérance : `random_state=1, max_iter=300E0, tol=1E-8, solver='lbfgs'` 

l'accuracy est de 0.72, elle a augmentée un petit peu, on arrive encore à bien reconnaitre les battements normaux et inconnus mais on a toujours du mal à reconnaitre les battements de fusion.

![output_neurone_300E3_1e-8_lbfgs](/home/n7student/3A/DataAnalysis/data-analysis/img/output_neurone_300E3_1e-8_lbfgs.png)

En testant le solveur SGD, il semble moins efficace sur notre jeu de données, il faut plus d'itérations que les autres pour arriver à converger +950

#### Quatrième partie : Etude perso, comparer ça avec telle méthode, critiquer les résultats, comment améliorer les résultats, Quelle information est ratée ? (chaque méthode à ses paramètres, donc on peut custom les paramètres)

##### Experience avec un autre algorithme pour DTW :

On associe les scores trouvés à la bonne classe. Ensuite on calcule la moyenne des scores pour chaque classe, et on retient la classe qui posséde le score moyen le plus fabile.

Sur un jeu de donnée de 10 segments de test, 20 segments d'apprentissage, et 3 classes.

Accuracy : 0.46

![cursedperclass](./img/cursedperclass.png)

Contrairement à l'algo classique, on reconnait moins les battements normaux, mais plus les battements ectopiques ventriculaires

##### Experience avec 5 classes et algorithme classique DTW :

On a utilisé 5 classes avec l'algorithme classique (récupère le meilleur score, pas la moyenne du meilleur)

Accuracy : 0.72

 ![output_dtw_simple_confusion_matrix_5_classes](img/output_dtw_simple_confusion_matrix_5_classes.png)

L'algorithme est bien plus précis avec 5 classes, on passe de 0.63 d'accuracy à 0.72 en général, et on confirme que pour les battements normaux on est beaucoup plus précis (de 0.75 a 0.90) comme prévu.

##### Expériences sur ACP et kppv

Avec 5 classes et 20 données dans le jeu de test et d'apprentissage:

| Nombres de dimensions | Nombre de voisins | Ratio de variance | Précision des prédictions |
| --------------------- | ----------------- | ----------------- | ------------------------- |
| 3                     | 1                 | 0.67              | 0.50                      |
| 3                     | 3                 | 0.67              | 0.46                      |
| 3                     | 5                 | 0.67              | 0.44                      |
| 14                    | 1                 | 0.90              | 0.66                      |
| 14                    | 3                 | 0.90              | 0.58                      |
| 14                    | 5                 | 0.90              | 0.60                      |
| 23                    | 1                 | 0.95              | 0.68                      |
| 23                    | 3                 | 0.95              | 0.62                      |
| 23                    | 5                 | 0.95              | 0.56                      |
| 42                    | 1                 | 0.99              | 0.70                      |
| 42                    | 3                 | 0.99              | 0.66                      |
| 42                    | 5                 | 0.99              | 0.58                      |

On peut noter comme remarque que forcément quand on augmente les dimensions, on devient forcément meilleur sur le ratio de variance, mais on augmente de beaucoup la taille des données et le temps de calcul, donc il y a un équilibre à avoir à garder en tête.

En ayant Taille_Test = 150 et Taille_App = 750, donc des tailles de données beaucoup plus élevées.

| Nombres de dimensions | Nombre de voisins | Ratio de variance | Précision des prédictions |
| --------------------- | ----------------- | ----------------- | ------------------------- |
| 3                     | 1                 | 0.62              | 0.693                     |
| 3                     | 3                 | 0.62              | 0.710                     |
| 3                     | 5                 | 0.62              | 0.713                     |
| 3                     | 10                | 0.62              | 0.743                     |
| 3                     | 20                | 0.62              | 0.724                     |
| 3                     | 30                | 0.62              | 0.719                     |
| 18                    | 1                 | 0.90              | 0.864                     |
| 18                    | 3                 | 0.90              | 0.839                     |
| 18                    | 5                 | 0.90              | 0.827                     |
| 18                    | 10                | 0.90              | 0.805                     |
| 18                    | 20                | 0.90              | 0.779                     |
| 18                    | 30                | 0.90              | 0.757                     |
| 30                    | 1                 | 0.95              | 0.863                     |
| 30                    | 3                 | 0.95              | 0.841                     |
| 30                    | 5                 | 0.95              | 0.833                     |
| 30                    | 10                | 0.95              | 0.811                     |
| 30                    | 20                | 0.95              | 0.779                     |
| 30                    | 30                | 0.95              | 0.761                     |
| 69                    | 1                 | 0.99              | 0.865                     |
| 69                    | 3                 | 0.99              | 0.841                     |
| 69                    | 5                 | 0.99              | 0.828                     |
| 69                    | 10                | 0.99              | 0.807                     |
| 69                    | 20                | 0.99              | 0.783                     |
| 69                    | 30                | 0.99              | 0.753                     |

On voit de prime abord qu'il faut plus de dimensions pour atteindre les même score de variance. Mais qu'on peut atteindre un max beaucoup plus haut, on retiendra par exemple la matrice de confusion a 69 dimensions avec le score de 0.865

![output_knn_BIGDATA_69d_1n](/home/n7student/3A/DataAnalysis/data-analysis/img/output_knn_BIGDATA_69d_1n.png)



##### Expériences sur forêts aléatoires

Tout d'abord en augmentant la taille des bases de données de test à 150 et d'apprentissages à 750:

`max_depth=None`, `n_estimators=100` et `max_features='auto'`  => score = 0.872

![output_knn_BIGDATA_69d_1n](/home/n7student/3A/DataAnalysis/data-analysis/img/output_knn_BIGDATA_69d_1n.png)

On peut d'ores et déjà observer une précision plus élevées que quasiment toutes les autres méthodes utilisées jusqu'à présent. ça rejoint l'observation que ce type de méthode est l'une des plus puissantes.

En augmentant le nombre d'estimateurs à 10000 on peut observer un score qui augmente à 0.876. Mais un temps d’exécution assez long. De plus il faut noter que des exécutions répétées donnent un score légèrement différent à chaque fois

##### Expériences sur réseaux de neurones

Encore en augmentant les jeux de données comme précédemment: score de 0.844

On également peut jouer sur les mêmes paramètres que sur l'application vu sur le playground de tensorflow

![output_neurone_BIGDATA_300E3_1e-8_lbfgs](/home/n7student/3A/DataAnalysis/data-analysis/img/output_neurone_BIGDATA_300E3_1e-8_lbfgs.png)

On peut constater que sur toutes les méthodes, ce sont souvent les battements ectopiques ventriculaires et les battements de fusion qui sont confondus.

##### Conclusion

Globalement on a pu voir différentes méthodes qui toutes ont leur spécificités et leur cas d'utilisation, en fonction de la taille des données que l'on possède, du temps d’exécution demandé, du nombres de classes à identifier, on va préférer choisir certaines méthodes. En l’occurrence, celui de la forêt aléatoire semble particulièrement performant.